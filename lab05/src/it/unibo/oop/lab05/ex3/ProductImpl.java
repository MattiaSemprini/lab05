package it.unibo.oop.lab05.ex3;

public class ProductImpl implements Product {

	private String name;
	private double qty;
	
	public ProductImpl(String i,double y) {
		this.name = i;
		this.qty = y;
	}
	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public double getQuantity() {
		return this.qty;
	}

}
