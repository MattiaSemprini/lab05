/**
 * 
 */
package it.unibo.oop.lab05.ex3;

import java.util.Set;
import java.util.HashSet;
import java.util.LinkedHashSet;

/**
 * @author pc
 *
 */
public class WarehouseImpl implements Warehouse {

	/* (non-Javadoc)
	 * @see it.unibo.oop.lab05.ex3.Warehouse#addProduct(it.unibo.oop.lab05.ex3.Product)
	 */
	
	private Set<Product> Prodotti = new LinkedHashSet<>();
	
	@Override
	public void addProduct(Product p) {
		// TODO Auto-generated method stub
		if(p != null)
		Prodotti.add(p);
	}

	/* (non-Javadoc)
	 * @see it.unibo.oop.lab05.ex3.Warehouse#allNames()
	 */
	@Override
	public Set<String> allNames() {
		Set<String> out = new HashSet<>();
		Prodotti.forEach(t -> out.add(t.getName() ));
		return out;
		
	}

	/* (non-Javadoc)
	 * @see it.unibo.oop.lab05.ex3.Warehouse#allProducts()
	 */
	@Override
	public Set<Product> allProducts() {
		
		Set<Product> out = new HashSet<>();
		Prodotti.forEach(t -> out.add(t ));
		return out;
	}

	/* (non-Javadoc)
	 * @see it.unibo.oop.lab05.ex3.Warehouse#containsProduct(it.unibo.oop.lab05.ex3.Product)
	 */
	@Override
	public boolean containsProduct(Product p) {
		if(p != null) {
		return Prodotti.contains(p);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see it.unibo.oop.lab05.ex3.Warehouse#getQuantity(java.lang.String)
	 */
	@Override
	public double getQuantity(String name) {
		if(name != null) {	
		double i = 0;
		for(Product t : this.Prodotti ) {
			if(t.getName().equals(name)) {
				i += t.getQuantity();
			}
		}

		return i == 0 ? -1 : i;
	}
		return -1;
	}
	
}
