package it.unibo.oop.lab05.ex2;

import java.util.Comparator;
import java.util.TreeSet;

public class TreeSetComparetor extends TreeSet<String> implements Comparator<String>  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public int compare(final String arg0, final String arg1) {
	
		if(Integer.parseInt(arg0) > Integer.parseInt(arg1)) {
		
			return Integer.parseInt(arg0);
	}
		
	else {
		return Integer.parseInt(arg1);
		}
	
	}

}
